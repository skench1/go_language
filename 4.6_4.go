package main

import (
	"fmt"
)

func main() {
	fmt.Println("Введите значение 3х чисел и программа выведет кол-во цифр больше 5")
	var a, b, c, i int = 0, 0, 0, 0
	fmt.Scan(&a)
	fmt.Scan(&b)
	fmt.Scan(&c)

	if a > 5 {
		i++
	} else if b > 5 {
		i++
	} else if c > 5 {
		i++
	}
	
	if i > 0 {
		fmt.Println("Количество цифр больше 5: ", i)
	} else {
		fmt.Println("Нет цифр больше 5")
	}
}
