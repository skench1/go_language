package main

import (
	"fmt"
	"os"
)

func main() {
	passingPoints := 275
	var algebra, russianLanguage, physics int = 0, 0, 0
	fmt.Fscan(os.Stdin, &algebra)
	if algebra < 0 {
		fmt.Println("Вы ввели отрицательное значение")
	} else if algebra > 100 {
		fmt.Println("Введено не корректное значение. Больше 100")
	}
	fmt.Fscan(os.Stdin, &russianLanguage)
	if algebra < 0 {
		fmt.Println("Вы ввели отрицательное значение")
	} else if russianLanguage > 100 {
		fmt.Println("Введено не корректное значение. Больше 100")
	}
	fmt.Fscan(os.Stdin, &physics)
	if algebra < 0 {
		fmt.Println("Вы ввели отрицательное значение")
	} else if physics > 100 {
		fmt.Println("Введено не корректное значение. Больше 100")
	}
	result := (algebra + russianLanguage + physics)
	if result < passingPoints {
		fmt.Println("Вы не набрали необходимого кол-ва балов")

	} else {
		fmt.Println("Поздравляю, вы зачислены в Слизерин")
	}
}
